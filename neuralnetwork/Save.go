package neuralnetwork

import (
	"encoding/json"
	"os"
)

type data struct {
	BinaryNeurons      [][]byte
	Layers             []int
	ActivationFunction ActivationFunction
	LearningRate       float64
}

//Save saves neural network
func (network *NeuralNetwork) Save(path string) error {
	binaryNeurons := [][]byte{}
	for i := range network.neurons {
		binary, _ := network.neurons[i].MarshalBinary()
		binaryNeurons = append(binaryNeurons, binary)
	}

	data := data{
		BinaryNeurons:      binaryNeurons,
		ActivationFunction: network.activationFunction,
		Layers:             network.layers,
		LearningRate:       network.learningRate,
	}

	result, err := json.Marshal(data)
	if err != nil {
		return err
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}

	file.Write(result)

	file.Close()

	return nil
}
