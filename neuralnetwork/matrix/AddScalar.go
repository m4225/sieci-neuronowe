package matrix

import "gonum.org/v1/gonum/mat"

//AddScalar adds  scalar value to every field in matrix
func AddScalar(matrix mat.Matrix, scalar float64) mat.Matrix {
	m, n := matrix.Dims()
	tempArray := make([]float64, m*n)
	for j := range tempArray {
		tempArray[j] = scalar
	}

	matrixProd := mat.NewDense(m, n, tempArray)

	return Add(matrix, matrixProd)
}
