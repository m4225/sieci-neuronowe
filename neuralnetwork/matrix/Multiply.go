package matrix

import "gonum.org/v1/gonum/mat"

//Multiply multiplies matrixes
func Multiply(matrixA, matrixB mat.Matrix) mat.Matrix {
	m1, _ := matrixA.Dims()
	_, n2 := matrixB.Dims()
	matrixProd := mat.NewDense(m1, n2, nil)
	matrixProd.Product(matrixA, matrixB)

	return matrixProd
}
