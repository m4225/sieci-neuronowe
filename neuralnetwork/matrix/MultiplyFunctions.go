package matrix

import "gonum.org/v1/gonum/mat"

//MultiplayFunctions function
func MultiplayFunctions(matrixA, matrixB mat.Matrix) mat.Matrix {
	m, n := matrixA.Dims()
	matrixProd := mat.NewDense(m, n, nil)
	matrixProd.MulElem(matrixA, matrixB)

	return matrixProd
}
