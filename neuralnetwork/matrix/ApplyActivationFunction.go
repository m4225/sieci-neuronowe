package matrix

import "gonum.org/v1/gonum/mat"

//ApplyActivationFunction applies activation function do matrix
func ApplyActivationFunction(matrix mat.Matrix, activationFunction func(float64) float64) mat.Matrix {
	m, n := matrix.Dims()
	matrixProd := mat.NewDense(m, n, nil)
	matrixProd.Apply(func(i, j int, v float64) float64 {
		return activationFunction(v)
	}, matrix)

	return matrixProd
}
