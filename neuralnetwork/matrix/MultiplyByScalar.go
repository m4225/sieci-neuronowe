package matrix

import "gonum.org/v1/gonum/mat"

//MultiplyByScalar multiplies matrix by scalar value
func MultiplyByScalar(matrix mat.Matrix, scalar float64) mat.Matrix {
	m, n := matrix.Dims()
	matrixProd := mat.NewDense(m, n, nil)
	matrixProd.Scale(scalar, matrix)

	return matrixProd
}
