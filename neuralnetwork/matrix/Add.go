package matrix

import "gonum.org/v1/gonum/mat"

//Add function adding matrixes
func Add(matrixA, matrixB mat.Matrix) mat.Matrix {
	m, n := matrixA.Dims()
	matrixProd := mat.NewDense(m, n, nil)
	matrixProd.Add(matrixA, matrixB)

	return matrixProd
}
