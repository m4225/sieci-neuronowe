package matrix

import "gonum.org/v1/gonum/mat"

//Sub subs matrix B from matrix A
func Sub(matrixA, matrixB mat.Matrix) mat.Matrix {
	m, n := matrixA.Dims()
	matrixProd := mat.NewDense(m, n, nil)
	matrixProd.Sub(matrixA, matrixB)

	return matrixProd
}
