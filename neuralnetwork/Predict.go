package neuralnetwork

import (
	"gitlab.com/m4225/sieci-neuronowe/neuralnetwork/matrix"
	"gonum.org/v1/gonum/mat"
)

//Predict function predicting result with neural network
func (network *NeuralNetwork) Predict(inputFloats []float64) ([]float64, error) {
	if network.modifier != 0 {
		for i := 0; i < network.modifier; i++ {
			inputFloats = append(inputFloats, 1)
		}
	}

	var result mat.Matrix = mat.NewDense(len(inputFloats), 1, inputFloats)

	for i := range network.neurons {
		inputs := matrix.Multiply(network.neurons[i], result)
		result = matrix.ApplyActivationFunction(inputs, network.activationFunc)
	}

	wynik := []float64{}
	r, _ := result.Dims()
	for i := 0; i < r; i++ {
		wynik = append(wynik, result.At(i, 0))
	}

	return wynik, nil
}
