package neuralnetwork

import "math"

type ActivationFunction string

const (
	AF_LINEAR             = "LINEAR"
	AF_THRESHHOLD         = "THRESHHOLD"
	AF_THRESHHOLD_BIPOLAR = "THRESHHOLD_BIPOLAR"
	AF_SIGMOID            = "SIGMOID"
)

func getActivationFunc(activationFunction ActivationFunction) func(float64) float64 {
	switch activationFunction {
	case AF_LINEAR:
		return linear
	case AF_THRESHHOLD:
		return threshold
	case AF_THRESHHOLD_BIPOLAR:
		return thresholdBipolar
	case AF_SIGMOID:
		return sigmoid
	}

	return nil
}

func linear(x float64) float64 {
	return x
}

func threshold(x float64) float64 {
	if x >= 0 {
		return 1.0
	}

	return 0.0
}

func thresholdBipolar(x float64) float64 {
	if x >= 0 {
		return 1.0
	}

	return -1.0
}

func sigmoid(x float64) float64 {
	return 2.0/(1+math.Exp(-1*x)) - 1
}
