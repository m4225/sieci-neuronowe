package neuralnetwork

import "gonum.org/v1/gonum/mat"

type NeuralNetwork struct {
	learningRate       float64
	activationFunction ActivationFunction
	isSigmoid          bool
	activationFunc     func(x float64) float64
	layers             []int
	neurons            []*mat.Dense
	modifier           int
}
