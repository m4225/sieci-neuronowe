package neuralnetwork

import (
	"gitlab.com/m4225/sieci-neuronowe/neuralnetwork/matrix"
	"gonum.org/v1/gonum/mat"
)

func (network *NeuralNetwork) Train(epochs int, inputData [][]float64, targetData [][]float64) {
	if network.modifier != 0 {
		for k := range inputData {
			for i := 0; i < network.modifier; i++ {
				inputData[k] = append(inputData[k], 1)
			}
		}
	}

	for i := 0; i < epochs; i++ {
		for j := range inputData {
			network.train(inputData[j], targetData[j])
		}
	}
}

//Train trains neural network
func (network *NeuralNetwork) train(inputData []float64, targetData []float64) {

	outputs, hiddenOutputs := network.predictWithHidden(inputData)

	var targets mat.Matrix = mat.NewDense(len(targetData), 1, targetData)

	errors := matrix.Sub(targets, outputs)
	// fmt.Println(errors)

	for i := len(network.neurons) - 1; i >= 0; i-- {
		newErrors := matrix.Multiply(network.neurons[i].T(), errors)

		m1 := errors
		if network.isSigmoid {
			m1 = matrix.MultiplayFunctions(errors, sigmoidPrime(hiddenOutputs[i+1]))
		}

		m2 := matrix.Multiply(m1, hiddenOutputs[i].T())

		network.neurons[i] = matrix.Add(network.neurons[i],
			matrix.MultiplyByScalar(m2, network.learningRate)).(*mat.Dense)

		errors = newErrors
	}

	return
}

func sigmoidPrime(m mat.Matrix) mat.Matrix {
	rows, _ := m.Dims()
	o := make([]float64, rows)
	for i := range o {
		o[i] = 1
	}
	ones := mat.NewDense(rows, 1, o)
	return matrix.Sub(ones, matrix.Sub(m, m))
}
