package neuralnetwork

import "gonum.org/v1/gonum/mat"

func (network *NeuralNetwork) Mirror() {
	d1 := network.neurons[0]
	_, c := d1.Caps()
	result := make([][]float64, c)
	for i := range result {
		result[i] = make([]float64, c)
	}
	for j := 0; j < c; j++ {
		for k := 0; k < j; k++ {
			elem := d1.At(j, k)
			result[k][j] = elem
			result[j][k] = elem
		}
	}

	r := []float64{}
	for i := range result {
		r = append(r, result[i]...)
	}

	network.neurons[0] = mat.NewDense(c, c, r)
}
