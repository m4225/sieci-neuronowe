package neuralnetwork

import "gonum.org/v1/gonum/mat"

func Create(weights, learningRate float64, ifThreshhold, isSigmoid bool, activationFunction ActivationFunction, layers ...int) *NeuralNetwork {
	result := &NeuralNetwork{
		learningRate:   learningRate,
		layers:         layers,
		activationFunc: getActivationFunc(activationFunction),
		neurons:        make([]*mat.Dense, len(layers)-1),
		isSigmoid:      isSigmoid,
	}

	if ifThreshhold {
		result.modifier = 1
	}

	for i := 0; i < len(layers)-1; i++ {
		modifier := result.modifier
		if i != 0 {
			modifier = 0
		}

		result.neurons[i] = mat.NewDense(
			layers[i+1],
			layers[i]+modifier,
			randomizeWeights(
				(layers[i]+modifier)*(layers[i+1]),
				weights))
	}

	return result
}
