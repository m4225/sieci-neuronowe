package neuralnetwork

import (
	"gitlab.com/m4225/sieci-neuronowe/neuralnetwork/matrix"
	"gonum.org/v1/gonum/mat"
)

//PredictWithHidden function predicting result with neural network
func (network *NeuralNetwork) predictWithHidden(inputData []float64) (mat.Matrix, []mat.Matrix) {
	var result mat.Matrix = mat.NewDense(len(inputData), 1, inputData)
	tempR := result

	results := []mat.Matrix{tempR}

	for i := range network.neurons {
		inputs := matrix.Multiply(network.neurons[i], result)
		temp := matrix.ApplyActivationFunction(inputs, network.activationFunc)
		results = append(results, temp)

		result = temp
	}

	return result, results
}
