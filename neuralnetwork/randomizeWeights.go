package neuralnetwork

import (
	"math"

	"gonum.org/v1/gonum/stat/distuv"
)

func randomizeWeights(size int, v float64) []float64 {
	dist := distuv.Uniform{
		Min: -math.Abs(v),
		Max: math.Abs(v),
	}

	data := make([]float64, size)
	for i := 0; i < size; i++ {
		data[i] = dist.Rand()
	}
	return data
}
