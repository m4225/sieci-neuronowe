package neuralnetwork

import (
	"encoding/json"
	"io/ioutil"

	"gonum.org/v1/gonum/mat"
)

//Load function loading neural network
func (network *NeuralNetwork) Load(path string) error {
	jData, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	var data data
	err = json.Unmarshal(jData, &data)
	if err != nil {
		return err
	}

	network.neurons = make([]*mat.Dense, len(data.BinaryNeurons))
	for i := range network.neurons {
		network.neurons[i] = &mat.Dense{}
		err = network.neurons[i].UnmarshalBinary(data.BinaryNeurons[i])
		if err != nil {
			return err
		}
	}

	network.layers = data.Layers
	network.learningRate = data.LearningRate
	network.activationFunction = data.ActivationFunction
	network.activationFunc = getActivationFunc(data.ActivationFunction)

	return nil
}
