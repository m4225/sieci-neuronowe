package neuralnetwork

//Predict function predicting result with neural network
func (network *NeuralNetwork) PredictRecurrent(output []float64, iterations *int) ([]float64, int, error) {
	stab := 0
	for i := 0; iterations == nil || i < *iterations; i++ {
		newOutput, err := network.Predict(output)
		if err != nil {
			return nil, 0, err
		}

		if equal(output, newOutput) {
			stab++
		}

		if stab == 10 {
			return output, i, nil
		}

		output = newOutput
	}

	return output, *iterations, nil
}

func equal(v1, v2 []float64) bool {
	for i := range v1 {
		if v1[i] != v2[i] {
			return false
		}
	}

	return true
}
