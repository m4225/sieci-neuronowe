package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"math/rand"
	"time"

	"gitlab.com/m4225/sieci-neuronowe/neuralnetwork"
)

func main() {
	siecHopfieldaLitery()
}

func neuronLiniowy() {
	nn := neuralnetwork.Create(0.1, 0.1, false, false, neuralnetwork.AF_LINEAR, 5, 1)

	nn.Train(10, [][]float64{
		{0.447, 0.447, 0.447, 0.447, 0.447},
		{-0.447, -0.447, -0.447, -0.447, -0.447},
		{0.447, 0.447, -0.447, -0.447, -0.447},
		{-0.447, -0.447, 0.447, 0.447, 0.447},
	}, [][]float64{{1}, {-1}, {0.8}, {-0.8}})

	fmt.Println(nn.Predict([]float64{1, 1, 1, 1, 1}))
}

func siecLiniowa() {
	nn := neuralnetwork.Create(0.1, 0.1, false, false, neuralnetwork.AF_LINEAR, 5, 3)

	nn.Train(10000, [][]float64{
		{0.346, 0.462, 0.346, 0.462, 0.577},
		{0.196, -0.392, 0.196, -0.392, -0.784},
		{0.525, 0.263, 0.657, 0.394, 0.263},
		{0, -0.229, 0, -0.688, -0.688},
	}, [][]float64{{1, -1, 0.8}, {-1, 0.8, -0.8}, {0.8, -0.8, 1}, {-0.8, 1, -1}})

	fmt.Println(nn.Predict([]float64{0.196, -0.392, 0.196, -0.392, -0.784}))
}

func siecNieliniowa1() {
	nn := neuralnetwork.Create(0.1, 0.1, true, false, neuralnetwork.AF_THRESHHOLD_BIPOLAR, 5, 3)

	nn.Train(20, [][]float64{
		{3, 4, 4, 4, 5},
		{1, -2, 1, -2, -2},
		{-3, -2, -3, 2, 3},
	}, [][]float64{{1, -1, -1}, {-1, 1, -1}, {-1, -1, 1}})

	fmt.Println(nn.Predict([]float64{3, 4, 4, 4, 5}))
}

func siecNieliniowa2() {
	nn := neuralnetwork.Create(0.1, 0.1, true, false, neuralnetwork.AF_THRESHHOLD_BIPOLAR, 2, 3)

	nn.Train(1000, [][]float64{
		{-1, 1},
		{5, -5},
		{7, -7},
	}, [][]float64{{1, -1, -1}, {-1, 1, -1}, {-1, -1, 1}})

	fmt.Println(nn.Predict([]float64{-1, 1}))
}

func aproksymacjaJednowarstwowa() {
	in, target := make([]float64, 14), make([]float64, 14)
	start := 0.4
	for i := range in {
		in[i] = start
		target[i] = math.Sin(start)
		start += 0.4
	}

	nn := neuralnetwork.Create(1, 0.1, false, false, neuralnetwork.AF_SIGMOID, 14, 14)

	nn.Train(100, [][]float64{in}, [][]float64{target})

	in2, target2 := make([]float64, 14), make([]float64, 14)
	for i := range in {
		in2[i] = in[i] + 0.05
		target2[i] = math.Sin(in2[i])
	}

	result, _ := nn.Predict(in)
	for i := range result {
		fmt.Println(result[i], target[i])
	}
}

func aproksymacjaJednowarstwowa2() {
	in, target := make([]float64, 14), make([]float64, 14)
	start := 0.4
	for i := range in {
		in[i] = start
		target[i] = math.Sin(start)
		start += 0.4
	}

	nn := neuralnetwork.Create(1, 0.01, false, true, neuralnetwork.AF_SIGMOID, 14, 14)

	nn.Train(100, [][]float64{in}, [][]float64{target})

	in2, target2 := make([]float64, 14), make([]float64, 14)
	for i := range in {
		in2[i] = in[i] + 0.05
		target2[i] = math.Sin(in2[i])
	}

	result, _ := nn.Predict(in)
	for i := range result {
		fmt.Println(result[i], target[i])
	}
}

func wstecznaPropagacjaBledow1() {
	nn := neuralnetwork.Create(1, 1.5, false, true, neuralnetwork.AF_SIGMOID, 5, 2, 1)

	in, target := [][]float64{
		{0, 0, 0, 0, 0},
		{1, 1, 1, 1, 1},
		{1, 1, 0, 0, 0},
		{1, 0, 1, 0, 1},
	}, [][]float64{{1}, {0}, {0.3}, {0.7}}

	nn.Train(100000, in, target)

	for i := range in {
		result, _ := nn.Predict(in[i])
		fmt.Println(result[0], target[i][0])
	}
}

func aproksymacjaWielowarstwowa() {
	in, target := make([]float64, 14), make([]float64, 14)
	start := 0.4
	for i := range in {
		in[i] = start
		target[i] = math.Sin(start)
		start += 0.4
	}

	nn := neuralnetwork.Create(1, 0.001, true, true, neuralnetwork.AF_SIGMOID, 14, 20, 14)

	nn.Train(10000, [][]float64{in}, [][]float64{target})

	in2, target2 := make([]float64, 14), make([]float64, 14)
	in3, target3 := make([]float64, 14), make([]float64, 14)
	for i := range in {
		in2[i] = in[i] + 0.05
		target2[i] = math.Sin(in2[i])

		in3[i] = in[i] - 0.05
		target3[i] = math.Sin(in3[i])
	}

	result, _ := nn.Predict(in2)
	for i := range result {
		fmt.Println(result[i], target2[i])
	}

	result, _ = nn.Predict(in3)
	for i := range result {
		fmt.Println(result[i], target3[i])
	}
}

func aproksymacjaWielowarstwowa2() {
	iterator := 0.01
	l := int(math.Floor(math.Pi / iterator))
	idxy := generujLosoweIdx(l)
	in, target := make([][]float64, l), make([][]float64, l)

	counter := 0
	for start := iterator; start < math.Pi; start += iterator {
		idx := idxy[counter]
		in[idx] = []float64{start}
		target[idx] = []float64{math.Sin(start)}
		counter++
	}

	nn := neuralnetwork.Create(0.001, 0.01, true, false, neuralnetwork.AF_SIGMOID, 1, 5, 1)

	fmt.Println(len(in))

	nn.Train(10, in, target)

	// in2, target2 := make([]float64, 14), make([]float64, 14)
	// in3, target3 := make([]float64, 14), make([]float64, 14)
	for i := range in {
		in2 := in[i][0] + 0.005

		in3 := in[i][0] - 0.005

		result, _ := nn.Predict([]float64{in2})
		fmt.Println(result[0], math.Sin(in2))

		result, _ = nn.Predict([]float64{in3})
		fmt.Println(result[0], math.Sin(in3))
	}
}

func siecHopfieldaLitery() {
	target, predict, l := wczytajDane(7)

	nn := neuralnetwork.Create(1, 0.1, false, false, neuralnetwork.AF_THRESHHOLD_BIPOLAR, l, l)

	nn.Train(1000, target, target)
	nn.Mirror()

	maxDistort := int(float64(l) * 0.1)
	maxIterations := 10000

	for litera, in := range predict {
		for i := 0; i < maxDistort; i++ {
			inDist := distort(in, i)
			start := time.Now()
			out, iter, err := nn.PredictRecurrent(inDist, &maxIterations)
			if err != nil {
				continue
			}

			fmt.Println("Litera:", litera, "Iteracje:", iter, "Distort:", i, "Czas:", time.Since(start), "Zgodny:", equalPercent(inDist, out), "%")
		}

	}
}

func generujLosoweIdx(max int) []int {
	wynik := make([]int, max)
	oznaczanie := make([]bool, max)

	for counter := 0; counter != max; counter++ {
		idx := rand.Int() % max
		for oznaczanie[idx] {
			idx = rand.Int() % max
		}

		wynik[counter] = idx
		oznaczanie[idx] = true
	}

	return wynik
}

func distort(wzorzec []float64, n int) []float64 {
	arr := make([]bool, len(wzorzec))
	for i := 0; i < n; i++ {
		idx := rand.Intn(len(wzorzec))
		for arr[idx] {
			idx = rand.Intn(len(wzorzec))
		}

		if wzorzec[idx] == -1 {
			wzorzec[idx] = 1
		} else {
			wzorzec[idx] = -1
		}
	}

	return wzorzec
}

func wczytajDane(n int) ([][]float64, map[string][]float64, int) {
	odp, _ := ioutil.ReadFile("litery.json")
	t := map[string][]string{}
	json.Unmarshal(odp, &t)

	wzorce := [][]float64{}
	wynik := make(map[string][]float64)
	length := 0
	iter := 0
	for litera, arr := range t {
		wzorceTemp := []float64{}
		for i := range arr {
			for j := range arr[i] {
				w := -1.0
				if arr[i][j] != ' ' {
					w = 1
				}
				wzorceTemp = append(wzorceTemp, w)
			}
		}

		wynik[litera] = wzorceTemp
		length = len(wzorceTemp)
		wzorce = append(wzorce, wzorceTemp)
		iter++
		if iter == n {
			break
		}
	}

	return wzorce, wynik, length
}

func equalPercent(v1, v2 []float64) int {
	diff := 0
	for i := range v1 {
		if v1[i] != v2[i] {
			diff++
		}
	}

	return int(float64(len(v1)-diff) * 100.0 / float64(len(v1)))
}
